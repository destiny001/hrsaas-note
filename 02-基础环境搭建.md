# 人力资源项目-准备工作

后台管理系统：方便维护后台数据库数据的网站系统

项目介绍：iHRM人力资源管理系统是一款在线HR人力资源管理系统，帮助企业管理人事信息、员工档案、绩效考核等

saas系统(软件服务系统)：为企业开发的一整套解决方案的软件，例如hr资源管理系统

最终会有两套网站：付费购买

1. 企业使用的hr管理系统
2. 针对hr管理系统提供功能的后台管理系统

![image-20200810012833240](images/image-20200810012833240.png)

## 1.0-element-ui

### 1.1-创建项目

1. 创建项目

```js
vue create element-demo
```

2. 选择依赖

```jsx
babel choose-vue-verision
```

3. cd切换路径
4. 运行项目

### 1.2-element介绍安装

1. 介绍：[element-ui](https://element.eleme.cn/#/zh-CN)

   快速实现后台管理系统的组件库

2. 安装：`npm i element-ui -S`

### 1.3-完整引入

1. 完整引入：[文档](https://element.eleme.cn/#/zh-CN/component/quickstart)

2. 使用组件

```jsx
<el-row>
  <el-button>默认按钮</el-button>
  <el-button type="primary">主要按钮</el-button>
  <el-button type="success">成功按钮</el-button>
  <el-button type="info">信息按钮</el-button>
  <el-button type="warning">警告按钮</el-button>
  <el-button type="danger">危险按钮</el-button>
</el-row>
```

3. 查看效果

![image-20220721112501103](./images/image-20220721112501103.png)

### 1.4-按需引入

[按需引入文档](https://element.eleme.cn/#/zh-CN/component/quickstart)

1. 装包

```js
npm install babel-plugin-component -D
```

2. 添加babel配置,在脚手架babel.config.js中

```js
"plugins": [
    [
      "component",
      {
        "libraryName": "element-ui",
        "styleLibraryName": "theme-chalk"
      }
    ]
  ]
```

3. 删除样式，按需引入组件

```js
import Vue from 'vue'
import App from './App.vue'
import { Button, Row } from 'element-ui'

Vue.use(Button)
Vue.use(Row)

Vue.config.productionTip = false

new Vue({
  render: h => h(App)
}).$mount('#app')
```

4. 重新启动项目：`npm run serve`

### 1.5 优化按需引入

1. 新建plugins/element

```js
import Vue from 'vue'
import { Button, Row } from 'element-ui'

Vue.use(Button)
Vue.use(Row)

```

2. main引入

```js
import Vue from 'vue'
import App from './App.vue'
import './plugins/element.js'

Vue.config.productionTip = false

new Vue({
  render: h => h(App)
}).$mount('#app')

```

## 2.0-vue-element-admin

vue-cli：脚手架快速生成vue项目所需要及工程化配置

**包含：**

+ vue项目基本依赖，例如vue、vuex、vue-router、less/sass、ts
+ 工程化：css文件的打包处理、vue文件的解析、js高级语法的转换、开启服务、代码格式化、代码压缩混淆等等

element-ui：组件库

**包含：**

+ 按钮组件、图标组件、表单组建、布局组件

### 2.1 介绍

vue-element-admin：快速生成基于vue脚手架和element-ui后台管理系统的一套解决方案

**包含：**

+ 登录功能、布局页面、excel导入导出、i18n多语言等等

[vue-element-admin官网](https://panjiachen.gitee.io/vue-element-admin-site/zh/) 

集成方案: [vue-element-admin ](https://github.com/PanJiaChen/vue-element-admin)

本项目的定位是后台集成方案，不太适合当基础模板来进行二次开发。因为本项目集成了很多你可能用不到的功能，会造成不少的代码冗余。如果你的项目不关注这方面的问题，也可以直接基于它进行二次开发。

基础模板: [vue-admin-template ](https://gitee.com/panjiachen/vue-admin-template)

开发更建议使用基础模版进行开发，只包含了常用的功能和解决方案

### 2.2 运行项目

**npm淘宝镜像**

> npm是非常重要的npm管理工具,由于npm的服务器位于国外, 所以一般建议 将 npm设置成国内的淘宝镜像

1. 设置淘宝镜像

```bash
$ npm config set registry  https://registry.npm.taobao.org/  #设置淘宝镜像地址
$ npm config get registry  #查看镜像地址
```

2. clone项目模版

```js
git clone git@gitee.com:panjiachen/vue-admin-template.git hrsaas
```

3. cd切换目录
4. 安装包
5. 运行项目

```bash
$ npm run dev #启动开发模式的服务
```

![image-20200708010741325](assets/image-20200708010741325.png)

项目运行完毕，浏览器会自动打开基础模板的登录页，如上图

### 2.3-目录介绍

**目录结构**

本项目已经为你生成了一个基本的开发框架，提供了涵盖中后台开发的各类功能和坑位，下面是整个项目的目录结构。

```bash
├── build                      # 构建相关
├── mock                       # 项目mock 模拟数据
├── public                     # 静态资源
│   │── favicon.ico            # favicon图标
│   └── index.html             # html模板
├── src                        # 源代码
│   ├── api                    # 所有请求
│   ├── assets                 # 主题 字体等静态资源
│   ├── components             # 全局公用组件
│   ├── icons                  # 项目所有 svg icons
│   ├── layout                 # 全局 layout 布局页面
│   ├── router                 # 路由
│   ├── store                  # 全局 store管理
│   ├── styles                 # 全局样式
│   ├── utils                  # 全局公用方法
│   ├── vendor                 # 三方库 无手动下载的
│   ├── views                  # views 所有页面
│   ├── App.vue                # 入口页面
│   ├── main.js                # 入口文件 加载组件 初始化等
│   └── permission.js          # 权限管理
│   └── settings.js          # 配置文件
├── tests                      # 测试
├── .env.xxx                   # 环境变量配置
├── .eslintrc.js               # eslint 配置项
├── .babelrc                   # babel-loader 配置
├── .travis.yml                # 自动化CI配置
├── vue.config.js              # vue-cli 配置
├── postcss.config.js          # postcss 配置
└── package.json               # package.json
```

此时,你可能会**眼花缭乱**, 因为生成的目录里面有太多的文件 我们在做项目时 其中最关注的就是**`src`**目录, 里面是所有的源代码和资源, 至于其他目录, 都是对项目的环境和工具的配置

**`本节任务`**： 按照操作和讲解步骤，进行拉取代码，安装依赖，运行项目，阅读目录和文件的操作

**`本节注意`** 需要注意自己的npm是否已经设置了淘宝镜像

## 3.0-项目运行机制

**`目标`**: 了解当前模板的基本运行机制和基础架构

> 眼花缭乱的目录和文件到底是怎么工作的？ 我们进行一下最基本的讲解，帮助大家更好的去理解和开发

```bash
├── src                        # 源代码
│   ├── api                    # 所有请求
│   ├── assets                 # 主题 字体等静态资源
│   ├── components             # 全局公用组件
│   ├── icons                  # 项目所有 svg icons
│   ├── layout                 # 全局 layout
│   ├── router                 # 路由
│   ├── store                  # 全局 store管理
│   ├── styles                 # 全局样式
│   ├── utils                  # 全局公用方法
│   ├── vendor                 # 公用vendor
│   ├── views                  # views 所有页面
│   ├── App.vue                # 入口页面
│   ├── main.js                # 入口文件 加载组件 初始化等
│   └── permission.js          # 权限管理
│   └── settings.js            # 配置文件
```

### 3.1main.js

![image-20200824153141764](assets/image-20200824153141764.png)

请注释掉**`mock数据`**的部分，删除src下的**`mock`**文件夹，我们开发的时候用不到模拟数据，如图

同时，请注释掉**`vue.config.js`**中的  **before: require('./mock/mock-server.js')**

![image-20200811013813693](assets/image-20200811013813693.png)

### 3.2 App

![image-20200824155103340](assets/image-20200824155103340.png)

### 3.3 permission

> src下，除了main.js还有两个文件，**`permission.js`** 和**`settings.js`**

**`permission.js`** 是控制页面登录权限的文件， 此处的代码没有经历构建过程会很难理解， 所以先将此处的代码进行注释，等我们构建权限功能时，再从0到1进行构建。

**注释代码**

![image-20200708014558617](assets/image-20200708014558617.png)

> **`settings.js`**则是对于一些项目信息的配置，里面有三个属性 **`title`**(项目名称)，**`fixedHeader`**（固定头部），**`sidebarLogo`**（显示左侧菜单logo）

**`settings.js`**中的文件在其他的位置会引用到，所以这里暂时不去对该文件进行变动

### 3.4 Vuex结构

> 当前的Vuex结构采用了模块形式进行管理共享状态，其架构如下

![image-20200824165153331](assets/image-20200824165153331.png)

> 其中app.js模块和settings.js模块，功能已经完备，不需要再进行修改。 user.js模块是我们后期需要重点开发的内容，所以这里我们将user.js里面的内容删除，并且导出一个默认配置

```js
export default  {
  namespaced: true,
  state: {},
  mutations: {},
  actions: {}
}
```

同时，由于getters中引用了user中的状态，所以我们将getters中的状态改为

```js
const getters = {
  sidebar: state => state.app.sidebar,
  device: state => state.app.device
}
export default getters

```

### 3.5 **icons**

> icons的结构如下

![image-20200824173239955](assets/image-20200824173239955.png)

> 以上就是vue-element-admin的基础和介绍,希望大家在这过程中体会 一个基础的模板运行机制

**`本节任务`**： 大家根据目录结构和设计图，对以上的内容进行了解

## 4.0 **scss**

> 该项目还使用了[scss](https://www.sass.hk/)作为css的扩展语言，在**`styles`**目录下，我们可以发现scss的相关文件，相关用法 我们[下一小节]() 进行讲解![image-20200824171327384](assets/image-20200824171327384.png)



[官方文档](https://www.sass.hk/)

> 首先注意,这里的sass和我们的scss是什么关系

sass和scss其实是**`一样的`**css预处理语言，SCSS 是 Sass 3 引入新的语法，其后缀名是分别为 .sass和.scss两种。
SASS版本3.0之前的后缀名为.sass，而版本3.0之后的后缀名.scss。
两者是有不同的，继sass之后scss的编写规范基本和css一致，sass时代是有严格的缩进规范并且没有‘{}’和‘；’。
而scss则和css的规范是一致的。

### 4.1 变量

`sass`使用`$`符号来标识变量 

```bash 
$highlight-color: #f90     
```

上面我们声明了一个 名为**`$highlight-color`**的变量, 我们可以把该变量用在任何位置

```bash
#app {
    background-color:  $highlight-color;
}     
```

以空格分割的多属性值也可以标识变量

```bash
$basic-border: 1px solid black;
```

```bash
#app {
    background-color:  $highlight-color;
    border: $basic-border
}     
```

### 4.2 变量范围

与`CSS`属性不同，变量可以在`css`规则块定义之外存在。当变量定义在`css`规则块内，那么该变量只能在此规则块内使用。如果它们出现在任何形式的`{...}`块中（如`@media`或者`@font-face`块），情况也是如此：

```bash
$nav-color: #F90;
nav {
  $width: 100px;
  width: $width;
  color: $nav-color;
  background-color: black
}

# 编译后 

nav {
  width: 100px;
  color: #F90;
  background-color: black;
}

```

在这段代码中，`$nav-color`这个变量定义在了规则块外边，所以在这个样式表中都可以像 `nav`规则块那样引用它。`$width`这个变量定义在了`nav`的`{ }`规则块内，所以它只能在`nav`规则块 内使用。这意味着是你可以在样式表的其他地方定义和使用`$width`变量，不会对这里造成影响。

### 4.3**嵌套语法**

和less一样,scss同样支持**`嵌套型`**的语法

```scss
#content {
    article {
      h1 { color: #1dc08a }
      p {  font-style: italic; }
    }
    aside { background-color: #f90 }
  }
```

转化后

```scss
#content article h1 {
  color: #1dc08a;
}

#content article p {
  font-style: italic;
}

#content aside {
  background-color: #f90;
}

```

### 4.4 **&父选择器**

假如你想针对某个特定子元素 进行设置

比如

```scss
  #content {
    article {
      h1 { color: #1dc08a }
      p {  font-style: italic; }
      a {
        color: blue;
        &:hover { color: red }
      }
    }
    aside { background-color: #f90 }
  }
```

 >学到这里,我们会发现scss和less有很多相似之处,最大的区别就在于声明变量的方式,less采用的是**`@变量名`**, 而scss采用的**`$变量名`**

此时,我们再来看一下模板中的 **`styles/variables.scss`**

![image-20201017230542301](assets/image-20201017230542301.png)

上述文件,实际上定义了我们的一些基础数值,方便大家在某个文件统一的处理.

**`本节任务`**：对scss进行了解和掌握

## 5.0 Git提交

**`目标`** 在[码云](https://gitee.com/)或者[github](https://github.com/)上建立相应的远程仓库,并将代码分支提交

1. 创建远程仓库
2. 删除本地项目.git

**`注意`**: 由于我们之前的项目是直接从 vue-element-admin **`克隆`**而来,里面拥有原来的提交记录,为了避免冲突, 先将原来的**`.git`**文件夹删除掉

mac电脑：shift+command+.显示隐藏文件

3. 本仓库初始化: `git init`

4. 本地提交: `git add .`  `git commit -m 说明文字`

5. 提交到远程仓库

   + 复制

     ```js
     git remote add origin git@gitee.com:destiny001/hrsaas.git
     git push -u origin "master"
     ```

     

## 6.0 公共资源

创建development分支

### 6.1 API清理

**Axios的拦截器介绍**

> 该项目采用了API的单独模块封装和axios拦截器的方式进行开发

axios的拦截器原理如下

![image-20200811012945409](assets/image-20200811012945409.png)

**axios拦截器**

axios作为网络请求的第三方工具, 可以进行请求和响应的拦截

**通过create创建了一个新的axios实例**

```js
// 创建了一个新的axios实例
const service = axios.create({
  baseURL: process.env.VUE_APP_BASE_API, // url = base url + request url
  // withCredentials: true, // send cookies when cross-domain requests
  timeout: 5000 // 超时时间
})
```

**请求拦截器**

请求拦截器主要处理 token的**`统一注入问题`**

```js
// axios的请求拦截器
service.interceptors.request.use(
  config => {
    // do something before request is sent

    if (store.getters.token) {
      // let each request carry token
      // ['X-Token'] is a custom headers key
      // please modify it according to the actual situation
      config.headers['X-Token'] = getToken()
    }
    return config
  },
  error => {
    // do something with request error
    console.log(error) // for debug
    return Promise.reject(error)
  }
)
```

**响应拦截器**

响应拦截器主要处理 返回的**`数据异常`** 和**`数据结构`**问题

```js
// 响应拦截器
service.interceptors.response.use(
  response => {
    const res = response.data

    // if the custom code is not 20000, it is judged as an error.
    if (res.code !== 20000) {
      Message({
        message: res.message || 'Error',
        type: 'error',
        duration: 5 * 1000
      })
      if (res.code === 50008 || res.code === 50012 || res.code === 50014) {
        // to re-login
        MessageBox.confirm('You have been logged out, you can cancel to stay on this page, or log in again', 'Confirm logout', {
          confirmButtonText: 'Re-Login',
          cancelButtonText: 'Cancel',
          type: 'warning'
        }).then(() => {
          store.dispatch('user/resetToken').then(() => {
            location.reload()
          })
        })
      }
      return Promise.reject(new Error(res.message || 'Error'))
    } else {
      return res
    }
  },
  error => {
    console.log('err' + error) // for debug
    Message({
      message: error.message,
      type: 'error',
      duration: 5 * 1000
    })
    return Promise.reject(error)
  }
)
```

> 这里为了后续更清楚的书写代码,我们将原有代码注释掉,换成如下代码

```js
// 导出一个axios的实例  而且这个实例要有请求拦截器 响应拦截器
import axios from 'axios'
const service = axios.create() // 创建一个axios的实例
service.interceptors.request.use() // 请求拦截器
service.interceptors.response.use() // 响应拦截器
export default service // 导出axios实例

```

### 6.2 **api模块清理**

我们习惯性的将所有的网络请求 放置在api目录下统一管理,按照模块进行划分

**单独封装代码**

```js
import request from '@/utils/request'

export function login(data) {
  return request({
    url: '/vue-admin-template/user/login',
    method: 'post',
    data
  })
}

export function getInfo(token) {
  return request({
    url: '/vue-admin-template/user/info',
    method: 'get',
    params: { token }
  })
}

export function logout() {
  return request({
    url: '/vue-admin-template/user/logout',
    method: 'post'
  })
}

```

上面代码中,使用了封装的request工具,每个接口的请求都单独**`导出`**了一个方法,这样做的好处就是,任何位置需要请求的话,可以直接引用我们导出的请求方法

为了后续更好的开发，我们可以先将**user.js**代码的方法设置为空，后续在进行更正

```js
// import request from '@/utils/request'

export function login(data) {

}

export function getInfo(token) {

}

export function logout() {

}

```

**提交代码**

**`本节任务`**： 将request和用户模块的代码进行清理，理解request和模块封装

### 6.3 图片和统一样式

**`目标`** 将一些公共的图片和样式资源放入到 规定目录中

> 我们已经将整体的基础模块进行了简单的介绍，接下来，我们需要将该项目所用到的图片和样式进行统一的处理

 **图片资源**

> 图片资源在课程资料的图片文件中，我们只需要将**`common`**文件夹拷贝放置到 **`assets`**目录即可

**样式**

> 样式资源在  资源/样式目录下

修改**`variables.scss`**

新增**`common.scss`**

我们在**`variables.scss`**添加了一些基础的变量值

我们提供了 一份公共的**`common.scss`**样式,里面内置了一部分内容的样式,在开发期间可以帮助我们快速的实现页面样式和布局

将两个文件放置到**styles**目录下，然后在**`index.scss`**中引入该样式

```scss
@import './common.scss'; //引入common.scss样式表 
```

## 7.0 提交代码

**提交代码**

**`本节注意`**：注意在scss文件中，通过**@import** 引入其他样式文件，需要注意最后加分号，否则会报错

**`本节任务`** 将公共资源的图片和样式放置到规定位置



